﻿/*
 Author: Marco Picariello & Sara Borriello
 Language: C#
 Description: this class contains the application logic of the genetic algorithm.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlgoritmoGenetico : MonoBehaviour {

    private HUDManager HUDman;

    public int populationSize;
    public float maxForce = 2000;
    public float minForce = 500;
    public float mutationRate = 0.01f;
    public float minMutationRate;
    public float maxMutationRate;

    private DartGenoma[] population;
    private DartGenoma[] population2;

    private int index = 0;
    private GameObject dart;

    public int generation = 0;

    private ArrayList fitnessNormalizzate = new ArrayList();
    
    //this method initializes the population array with random values.
    void Initialization() {
        for (int i =0; i< populationSize;i++) {
            population[i] = new DartGenoma(new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)), Random.Range(minForce, maxForce));
        }
    }

	void Start () {
        HUDman = GameObject.Find("HUD").GetComponent<HUDManager>();
	}

    //this method start the algorithm when user click "Start".
    public void StartAlgo() {
        population = new DartGenoma[populationSize];
        population2 = new DartGenoma[populationSize];
        generation = 0;
        index = 0;

        Initialization();
        ThrowArrow();
    }

    //this method throw a new arrow e manage the generations.
   public void ThrowArrow() {
        if (index < populationSize) {
            if (index == 0) {
                generation++;
                HUDman.SetGeneration(generation);
                HUDman.SetFinalGeneration(generation);
            }
            dart = Resources.Load("Freccetta", typeof(GameObject)) as GameObject;    
            dart = Instantiate(dart, gameObject.transform.position, Quaternion.identity);
            if (generation % 2 == 1) {
                dart.GetComponent<DartDNA>().genoma = population[index];
            } else {
                dart.GetComponent<DartDNA>().genoma = population2[index];
            }
            index++;
        } else {
            index = 0;
           
            if (generation % 2 == 1) {
                mutationRate = CalculateMutationRate(population);
                NewGeneration(population);
            } else {
                mutationRate = CalculateMutationRate(population2);
                NewGeneration(population2);
            }
            ThrowArrow();
        }
        
    }

    //this method creates the new generation
    void NewGeneration(DartGenoma[] population){
        fitnessNormalizzate.Clear();
        float total = 0;
        for (int i =0; i<population.Length; i++) {
				population[i].fitness =(1 /(population[i].fitness*population[i].fitness));
				total += population[i].fitness;
		}
		fitnessNormalizzate.Add(population[0].fitness / total);
		for(int i=1; i< population.Length; i++){
		    fitnessNormalizzate.Add( (float)fitnessNormalizzate[i-1] + (population[i].fitness / total));
		}	
		fitnessNormalizzate [population.Length-1]=1f;
        for(int k =0; k< population.Length; k++) {
            int index2 = fitnessNormalizzate.BinarySearch(Random.Range(0.0f, 1.0f));
            int index = fitnessNormalizzate.BinarySearch(Random.Range(0.0f, 1.0f));
            if (index < 0) {
                index = (~index);
            }
            if (index2 < 0) {
                index2 = (~index2);
            }
            Cross(index, index2, k);
        }
	}

    //this is the first method to calculate the cross between two element of the population.
    public void Cross(int padre, int madre,int index) {
        if (generation % 2 == 1) {
            population2[index] = new DartGenoma(new Vector3(population[madre].direction.x, population[padre].direction.y, population[madre].direction.z), population[padre].force);
            Mutation(population2[index]);
        } else {
            population[index] = new DartGenoma(new Vector3(population2[madre].direction.x, population2[padre].direction.y, population2[madre].direction.z), population2[padre].force);
            Mutation(population[index]);
        }
    }

   

   //this methon calculate the mutation.
    public void Mutation(DartGenoma elemento) {
        if (Random.Range(0.0f,1.0f) < mutationRate) {
            elemento.direction.Set(elemento.direction.x + Random.Range(-7f,7f), elemento.direction.y, elemento.direction.z);
        }
        if (Random.Range(0.0f, 1.0f) < mutationRate) {
            elemento.direction.Set(elemento.direction.x, elemento.direction.y + Random.Range(-7f, 7f), elemento.direction.z);
        }
        if (Random.Range(0.0f, 1.0f) < mutationRate) {
            elemento.direction.Set(elemento.direction.x, elemento.direction.y, elemento.direction.z + Random.Range(-7f, 7f));
        }
        if (Random.Range(0.0f, 1.0f) < mutationRate) {
            Debug.Log("voglio mutare la forza. Era " + elemento.force);
            elemento.force += Random.Range(-200f, 200f);
            Debug.Log(" è diventata: " + elemento.force);
        }
    }

    //this method calculate the value of the mutation rate based on the Average absolute deviation of the fitness.
    public float CalculateMutationRate(DartGenoma[] population) {
        float sum=0;
       float average = 0;
        for (int i =0; i< population.Length;i++) {
            sum += population[i].fitness;
        }
        average = sum / population.Length;
        //  coffVar = Mathf.Sqrt((1.0f / (population.Length - 1)) * (squareSum - (population.Length*(average*average))))/average;
        //  Debug.Log("coeff:" +coffVar);
        //  Debug.Log("Media: " + average + " squaresum: " + squareSum);
        //  Debug.Log("Varianza: " + ((1.0f / (population.Length - 1)) * (squareSum - (population.Length * (average * average)))));

        float scarti=0;
        float scartoMedioAssoluto = 0;
        for (int i=0;i<population.Length; i++) {
            scarti += Mathf.Abs(population[i].fitness - average);
        }
        scartoMedioAssoluto = 1.0f / population.Length * scarti;

        if (scartoMedioAssoluto >= 5) {
            return minMutationRate;
        } else if (scartoMedioAssoluto == 0) {
            return maxMutationRate;
        } else {
           // Debug.Log("mutation: " + (minMutationRate + ((maxMutationRate - minMutationRate) * (5 - scartoMedioAssoluto) / 5.0f)));
            return minMutationRate + ( (maxMutationRate - minMutationRate) * (5 - scartoMedioAssoluto) / 5.0f);
        }
    }
}
