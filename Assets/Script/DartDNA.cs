﻿/*
 Author: Marco Picariello & Sara Borriello
 Language: C#
 Description: this class manages the behavior of the dart.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DartDNA : MonoBehaviour {

    public DartGenoma genoma;
    public Vector3 centerOfMass;

    private GameObject target;
    private GameObject spawn;
    private HUDManager HUDman;

    void Start () {
        gameObject.GetComponent<Rigidbody>().centerOfMass = centerOfMass;
        gameObject.transform.rotation = Quaternion.Euler(genoma.direction);
        gameObject.GetComponent<Rigidbody>().AddForce(gameObject.transform.forward * -genoma.force);
        target = GameObject.Find("Target");
        spawn = GameObject.Find("DartSpawner");
        HUDman = GameObject.Find("HUD").GetComponent<HUDManager>();
    }

    //when it collides, if it does not hit the center it calculates the distance from 
    //the center of the target and calls the method to throw a new arrow or the game ends.
    private void OnCollisionEnter(Collision collision) {

        gameObject.GetComponent<MeshCollider>().enabled = false;
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;   
        genoma.fitness = (Vector3.Distance(collision.contacts[0].point, target.transform.position));

        HUDman.SetLastDirection(new Vector3((Mathf.Round(genoma.direction.x * 100)/ 100),
                                            (Mathf.Round(genoma.direction.y * 100) / 100),
                                            (Mathf.Round(genoma.direction.z * 100) / 100)));
        HUDman.SetLastForce(genoma.force);
        HUDman.SetLastFitness(genoma.fitness);

        if (genoma.fitness < HUDman.bestFitnessValue) {
            HUDman.bestFitnessValue = genoma.fitness;
            HUDman.SetBestDirection(new Vector3((Mathf.Round(genoma.direction.x * 100) / 100),
                                            (Mathf.Round(genoma.direction.y * 100) / 100),
                                            (Mathf.Round(genoma.direction.z * 100) / 100)));
            HUDman.SetBestForce(genoma.force);
            HUDman.SetBestFitness(genoma.fitness);
        }

        if (genoma.fitness <= 0.4f) {
            HUDman.HUD.enabled = false;
            HUDman.endindCanv.enabled = true;
            HUDman.SetFinalDirection(new Vector3((Mathf.Round(genoma.direction.x * 100) / 100),
                                            (Mathf.Round(genoma.direction.y * 100) / 100),
                                            (Mathf.Round(genoma.direction.z * 100) / 100)));
            HUDman.SetFinalForce(genoma.force);
            HUDman.SetTime();
        } else {
            spawn.GetComponent<AlgoritmoGenetico>().ThrowArrow();
            Destroy(gameObject);
        }
    }
}
