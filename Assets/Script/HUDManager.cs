﻿/*
 Author: Marco Picariello & Sara Borriello
 Language: C#
 Description: this class manages the user inferface.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HUDManager : MonoBehaviour {

    public Canvas HUD;
    public GameObject panelInfo;
    public Button info;
    public Button backInfo;    
    public Text generation;
    public Text bestDir;
    public Text bestForce;
    public Text bestFitness;
    public Text attDir;
    public Text attForce;
    public Text attFitness;

    public Canvas startCanv;
    public Button startAlgo;
    public InputField minMutation;
    public InputField maxForce;
    public InputField minForce;
    public InputField maxMutation;
    public InputField population;
    public Dropdown simSpeed;

    public Canvas endindCanv;
    public Text finalDir;
    public Text finalForce;
    public Text finalGeneration;
    public Text time;

    public float bestFitnessValue;

    private AlgoritmoGenetico algo;
    DateTime dt;
    DateTime currentdt;

    private int speed = 1;


    void Start () {
        bestFitnessValue = float.MaxValue;
        algo = GameObject.Find("DartSpawner").GetComponent<AlgoritmoGenetico>();
        panelInfo.SetActive(false);
        HUD.enabled = false;
        endindCanv.enabled = false;
        startCanv.enabled = true;
        SetDefault();
	}

    #region Main Menu Methods
    public void ClickStart() {
        float appoggio = 0;
        int appoggio2 = 0;

        if (float.TryParse(minMutation.text, out appoggio)) {
            if (appoggio < 0.0f || appoggio > 1.0f) {
                minMutation.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
                return;
            } else {
                minMutation.image.color = new Color(1.0f, 1.0f, 1.0f, 0.7450f);
                algo.minMutationRate = appoggio;
                appoggio = 0;
            }
        } else {
            minMutation.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
            return;
        }

        if (float.TryParse(maxMutation.text, out appoggio)) {
            if (appoggio < 0.0f || appoggio > 1.0f) {
                maxMutation.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
                return;
            } else {
                maxMutation.image.color = new Color(1.0f, 1.0f, 1.0f, 0.7450f);
                algo.maxMutationRate = appoggio;
                appoggio = 0;
            }
        } else {
            maxMutation.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
            return;
        }


        if (float.TryParse(maxForce.text, out appoggio)) {
            if (appoggio < 0.0f) {
                maxForce.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
                return;
            } else {
                maxForce.image.color = new Color(1.0f, 1.0f, 1.0f, 0.7450f);
                algo.maxForce = appoggio;
                appoggio = 0;
            }
        } else {
            maxForce.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
            return;
        }

        if (float.TryParse(minForce.text, out appoggio)) {
            if (appoggio < 0.0f || appoggio > algo.maxForce) {
                minForce.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
                return;
            } else {
                minForce.image.color = new Color(1.0f, 1.0f, 1.0f, 0.7450f);
                algo.minForce = appoggio;
                appoggio = 0;
            }
        } else {
            minForce.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
            return;
        }

        if (int.TryParse(population.text, out appoggio2)) {
            if (appoggio2 < 0) {
                population.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
                return;
            } else {
                population.image.color = new Color(1.0f, 1.0f, 1.0f, 0.7450f);
                algo.populationSize = appoggio2;
                appoggio = 0;
            }
        } else {
            population.image.color = new Color(0.8f, 0.45f, 0.45f, 0.7450f);
            return;
        }

        Time.timeScale = 1;
        Time.timeScale *= speed;
        startCanv.enabled = false;
        HUD.enabled = true;
        algo.StartAlgo();
        dt = DateTime.Now;
    }

    public void SetDefault() {
        minMutation.text = "" + 0.01f;
        maxMutation.text = "" + 0.1f;
        minForce.text = "" + 700;
        maxForce.text = "" + 3000;
        population.text = "" + 50;
    }

    public void Exit() {
        Application.Quit();
    }

    public void SetSpeed() {
        switch (simSpeed.value) {
            case 0:             
                speed = 1;
                break;
            case 1:
                speed = 2;
                break;
            case 2:
                speed = 4;
                break;
            default:
                break;

        }
    }
    #endregion

    #region HUD Methods

    public void ClickInfo() {
        panelInfo.SetActive(true);
        info.enabled = false;
    }

    public void ClickInterrupt() {
        startCanv.enabled = true;
        HUD.enabled = false;
        minMutation.text = "" + algo.minMutationRate;
        maxMutation.text = "" + algo.maxMutationRate;
        minForce.text = "" + algo.minForce;
        maxForce.text = "" + algo.maxForce;
        population.text = "" + algo.populationSize;
        attDir.text= "(0,0,0)";
        attFitness.text = ""+0;
        attForce.text = ""+0;
        bestDir.text = "(0,0,0)";
        bestFitness.text = "" + 0;
        bestForce.text = "" + 0;
        bestFitnessValue = float.MaxValue;
        GameObject[] frecce = GameObject.FindGameObjectsWithTag("Freccetta");
        foreach (GameObject g in frecce) {
            Destroy(g);
        }
    }

    public void ClickBackInfo() {
        panelInfo.SetActive(false);
        info.enabled = true;
    }

    public void SetGeneration(int gen) {
        generation.text = ""+gen;
    }

    public void SetBestDirection(Vector3 direction) {
        bestDir.text = "(" + direction.x + ", " + direction.y + ", " + direction.z + ")";
    }

    public void SetBestForce(float bestforce) {
        bestForce.text = "" + bestforce;
    }

    public void SetBestFitness(float bestFitnessValue) {
        bestFitness.text = "" + bestFitnessValue;
    }
    
    public void SetLastForce(float attforce) {
        attForce.text = "" + attforce;
    }

    public void SetLastDirection(Vector3 direction) {
        attDir.text = "(" + direction.x + ", " + direction.y + ", " + direction.z + ")";
    }

    public void SetLastFitness(float attfitness) {
        attFitness.text = "" + attfitness;
    }
    #endregion

    #region Ending Methods
    public void SetFinalDirection(Vector3 direction) {
        finalDir.text = "(" + direction.x + "," + direction.y + "," + direction.z + ")";
    }

    public void SetFinalForce(float force) {
        finalForce.text = "" + force;
    }

    public void SetFinalGeneration(int gen) {
        finalGeneration.text = ""+gen;
    }

    public void SetTime() {
        currentdt = DateTime.Now;
        time.text = currentdt.Subtract(dt).ToString();
    }

    public void Restart() {
        startCanv.enabled = true;
        HUD.enabled = false;
        dt = DateTime.Now;
        endindCanv.enabled = false;

        minMutation.text = "" + algo.minMutationRate;
        minMutation.text = "" + algo.maxMutationRate;
        minForce.text = "" + algo.minForce;
        maxForce.text = "" + algo.maxForce;
        population.text = "" + algo.populationSize;

        finalDir.text = "(0,0,0)";
        finalForce.text = "" + 0;
        finalGeneration.text = "" + 0;
        time.text = 0+":"+0+":"+0;

        attDir.text = "(0,0,0)";
        attFitness.text = "" + 0;
        attForce.text = "" + 0;

        bestDir.text = "(0,0,0)";
        bestFitness.text = "" + 0;
        bestForce.text = "" + 0;
        bestFitnessValue = float.MaxValue;

        GameObject[] frecce = GameObject.FindGameObjectsWithTag("Freccetta");
        foreach (GameObject g in frecce) {
            Destroy(g);
        }
    }

    #endregion

}
