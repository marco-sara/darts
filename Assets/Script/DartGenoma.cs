﻿/*
 Author: Marco Picariello & Sara Borriello
 Language: C#
 Description: this box contains all the information of the single dart.
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DartGenoma{

    public Vector3 direction;
    public float force;
    public float fitness;

    public DartGenoma(Vector3 direction, float force) {
        this.direction = direction;
        this.force = force;
    }

    public DartGenoma() {

    }
		
}
