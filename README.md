# Darts Genetic Algorithm

A genetic algorithm that learns how to play darts.


## Getting Started

To use the project, download the repository and import it in Unity3D.

## Usage

Once the project has started, it will be possible to manage a series of parameters that influence the genetic algorithm. These are :

* The range of the mutation rate
* The size of the population
* The range of force that can be applied to the darts
* The simulation speed

### In Game Interface

During the simulation it is possible to monitor a series of parameters such as:

* The generation number
* Direction, Force and Fitness of the best element
* Direction, Force and Fitness of the last element of the population

When a dart hits the center, the algorithm ends, showing:

* The total number of generations
* Direction and Force of the element that achieved the goal
* The total time of the simulation.


## The Algorithm

The population of the genetic algorithm is made out of darts that are thrown at a target. It's initialized with darts with a random genome.
The genome of the darts is composed by the force and the direction in which they are thrown (See DartGenoma.cs file)
Each dart at the time of the collision checks the distance as the crow flies from the collision point to the center of the target. This distance will be used as a fitness measure (See the DartsDNA.cs file).
When a dart collides with a surface, the next one is thrown, unless the center of the target is hit. In this case, the algorithm ends and reports the values of the last dart.
Let's explain the parts of the algorithm:

* **Crossover** : In this phase, first of all, two elements are selected from the population. They are chosen using a probability proportional to 1 / (hitDistance) ^ 2.
The new dart takes two components of the direction vector from one parent, while takes the remaining direction component and the force from the other.

* **Mutation** : When a new individual is generated there can be mutation to his genome.
These changes are managed by the mutation rate. It's calculated at each generation and its value varies according to the average absolute deviation of the darts fitness values. When the gap decreases the mutation rate increases and then decreases when the gap increases. This technique helps us to avoid being stuck in local minimums.


## Built With

* [Unity 2018.1.2f1](https://docs.unity3d.com/Manual/index.html) - The game engine.
* [C#](https://docs.microsoft.com/it-it/dotnet/csharp/) - The language used.


## Versioning

We use [BitBucket](https://bitbucket.org/) for versioning. 


## Authors

* **Marco Picariello** 
* **Sara Borriello** 